package com.j2core.sts.webcrawler.ui.servlets;

import com.j2core.sts.webcrawler.ui.dao.WorkerDB;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by sts on 4/10/17.
 */
public class StopNodeServlet  extends HttpServlet {

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws IOException, ServletException {

        WorkerDB workerDB = new WorkerDB();
        String nodeId = request.getParameter("nodeId");
        Integer id = null;

        try {

            id = Integer.parseInt(nodeId);
        }catch (NumberFormatException e) {
            e.getStackTrace();
        }

        HttpSession session = request.getSession();

        if (workerDB.stopNode(id)){

            session.setAttribute("stopNode", true);

        }else {

            session.setAttribute("stopNode", false);

        }

        response.sendRedirect("nodesmanage.jsp");

    }
}

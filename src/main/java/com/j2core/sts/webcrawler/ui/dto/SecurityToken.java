package com.j2core.sts.webcrawler.ui.dto;

import java.util.List;

/**
 * Created by sts on 1/20/17.
 */
public class SecurityToken {

    private UserData userData;

    private List<RolesGroup> permission;

    public SecurityToken(){

    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    public List<RolesGroup> getPermission() {
        return permission;
    }

    public void setPermission(List<RolesGroup> permission) {

        this.permission = permission;
    }

    @Override
    public String toString() {
        return "SecurityToken{" +
                "userData=" + userData +
                ", permission=" + permission +
                '}';
    }

    public String toPermissionString(){

        StringBuilder stringBuilder = new StringBuilder();
        for (RolesGroup role : permission){

            stringBuilder.append(" ").append(role.getGroupName()).append(" - ").append(role.getDescription()).append(";").append("\n");
            stringBuilder.append(System.lineSeparator());

        }

        return stringBuilder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SecurityToken token = (SecurityToken) o;

        return userData.equals(token.userData);

    }

    @Override
    public int hashCode() {
        return userData.hashCode();
    }
}

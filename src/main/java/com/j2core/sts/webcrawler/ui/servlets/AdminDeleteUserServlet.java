package com.j2core.sts.webcrawler.ui.servlets;

import com.j2core.sts.webcrawler.ui.dao.WorkerDB;
import com.j2core.sts.webcrawler.ui.dto.SecurityToken;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by sts on 3/22/17.
 */
public class AdminDeleteUserServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();
        SecurityToken userToken = (SecurityToken) session.getAttribute("userToken");
        WorkerDB workerDB = new WorkerDB();

        boolean result = workerDB.deleteUser(userToken);

        if (result){

            session.removeAttribute("userToken");
            session.setAttribute("remoteUser", true);

        }else {

            session.setAttribute("exception", true);

        }

        response.sendRedirect("admindeleteuser.jsp");

    }
}

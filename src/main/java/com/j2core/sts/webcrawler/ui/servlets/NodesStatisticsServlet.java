package com.j2core.sts.webcrawler.ui.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


/**
 * Created by sts on 1/19/17.
 */
public class NodesStatisticsServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String nodeId = request.getParameter("nodeId");
        Integer id = null;

        try {

            id = Integer.parseInt(nodeId);
        }catch (NumberFormatException e) {
            e.getStackTrace();
        }

        if (id !=  null) {

            HttpSession session = request.getSession();
            session.setAttribute("nodeId", id);
            session.setAttribute("correctId", true);
            session.setMaxInactiveInterval(10*60);

            response.sendRedirect("nodestatistics.jsp");

        }else {

            HttpSession session = request.getSession();
            session.setAttribute("nodeId", id);
            session.setAttribute("correctId", false);
            response.sendRedirect("nodestatistics.jsp");
        }

    }
}

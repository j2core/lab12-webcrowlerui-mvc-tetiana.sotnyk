package com.j2core.sts.webcrawler.ui.servlets;

import com.j2core.sts.webcrawler.ui.dao.WorkerDB;
import com.j2core.sts.webcrawler.ui.dto.SecurityToken;
import com.j2core.sts.webcrawler.ui.dto.UserData;
import com.j2core.sts.webcrawler.ui.service.CryptographerUserPassword;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by sts on 3/16/17.
 */
public class ChangeUserPassServlet extends HttpServlet {


    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();
        SecurityToken token = (SecurityToken) session.getAttribute("securityToken");
        String new1pass = request.getParameter("new1Password");
        String new2pass = request.getParameter("new2Password");
        String login = request.getParameter("login");
        String userPass = request.getParameter("oldPassword");

        if (new1pass.equals(new2pass)) {

            String oldPassword = CryptographerUserPassword.getSecurePassword(login + userPass);
            WorkerDB workerDB = new WorkerDB();

            SecurityToken oldToken = workerDB.getSecurityToken(login, oldPassword);

            if (oldToken.equals(token)) {

                String newPassword = CryptographerUserPassword.getSecurePassword(login + new1pass);

                UserData userData = oldToken.getUserData();

                if (workerDB.changeUserInformation(userData, userData.getUserName(), userData.getLogin(), newPassword)) {

                    SecurityToken newToken = workerDB.getSecurityToken(login, newPassword);

                    session.setAttribute("securityToken", newToken);
                    session.setMaxInactiveInterval(30 * 60);
                }

                response.sendRedirect("profile.jsp");

            } else {

                RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/index.jsp");

                requestDispatcher.include(request, response);

            }
        } else {

            session.setAttribute("changedPass", false);

            response.sendRedirect("changeuserpass.jsp");

        }
    }

}


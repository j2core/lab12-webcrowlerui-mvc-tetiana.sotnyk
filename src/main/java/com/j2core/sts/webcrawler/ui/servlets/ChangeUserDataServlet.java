package com.j2core.sts.webcrawler.ui.servlets;

import com.j2core.sts.webcrawler.ui.dao.WorkerDB;
import com.j2core.sts.webcrawler.ui.dto.SecurityToken;
import com.j2core.sts.webcrawler.ui.service.CryptographerUserPassword;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

/**
 * Created by sts on 3/14/17.
 */
public class ChangeUserDataServlet extends HttpServlet {


    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();
        SecurityToken token =(SecurityToken) session.getAttribute("securityToken");
        String newUserName = request.getParameter("newName");
        String newLogin = request.getParameter("newLogin");
        String login = request.getParameter("oldLogin");
        String userPass = request.getParameter("password");

        String oldPassword = CryptographerUserPassword.getSecurePassword(login + userPass);
        WorkerDB workerDB = new WorkerDB();

        SecurityToken oldToken = workerDB.getSecurityToken(login, oldPassword);

        if (oldToken.equals(token)) {

            String newPassword = CryptographerUserPassword.getSecurePassword(newLogin + userPass);

            if (workerDB.changeUserInformation(oldToken.getUserData(), newUserName, newLogin, newPassword)) {

                SecurityToken newToken = workerDB.getSecurityToken(newLogin, newPassword);

                session.setAttribute("securityToken", newToken);
                session.setMaxInactiveInterval(30 * 60);
            }

            response.sendRedirect("profile.jsp");

        } else {

            RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/index.jsp");

            requestDispatcher.include(request, response);

        }
    }
}

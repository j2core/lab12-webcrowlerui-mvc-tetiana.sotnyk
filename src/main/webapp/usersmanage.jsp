<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%--
  Created by IntelliJ IDEA.
  User: sts
  Date: 2/10/17
  Time: 12:37 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Manage users</title>
</head>
<body>
<%@include file="header.jsp" %>
<%@include file="dbconnect.jsp"%>

<nav class="w3-sidenav w3-white w3-card-2" style="width:15%">
    <h4>Management:</h4>
    <a href="management.jsp">Manege nodes</a>
    <a style="background-color: lightgray" href="usersmanage.jsp">Manage users</a>
    <br>
    <a href="LogoutServlet">Logout</a>
    <a href="index.jsp">Home</a>

</nav>

<div class="w3-container" style="margin-left: 15%">

        <sql:query dataSource="${snapshot}" var="adminRole">
            SELECT * from rolesGroup WHERE groupId = 1;
        </sql:query>

        <c:set var="contains" value="false" scope="page"/>

        <c:forEach var="row" items="${adminRole.rows}">

            <c:set var="contains" value="false" scope="page"/>

            <c:forEach var="entity" items="${sessionScope.securityToken.permission}" >

                <c:if test="${entity.groupName eq row.groupName }">

                    <c:set var="contains" value="true" scope="page" />

                </c:if>

            </c:forEach>

        </c:forEach>

        <c:if test="${contains}">

            <h4>Management user's information:</h4>
            <a href="adminaddnewuser.jsp">Add new user</a><br>
            <a href="admindeleteuser.jsp">Delete user</a><br>
            <a href="adminchangeuserdata.jsp">Change user's information</a><br>

        </c:if>

        <c:if test="${!contains}">

            <p style="color: red">You do not have enough rights to be present in this part of the site.</p>

        </c:if>

</div>

<%@include file="footer.jsp" %>
</body>
</html>

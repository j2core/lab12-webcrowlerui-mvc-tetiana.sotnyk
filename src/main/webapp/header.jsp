<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: sts
  Date: 1/30/17
  Time: 11:01 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Header</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    <style>
        .dropbtn {
            background-color: gray;
            color: cornflowerblue;
            padding: 16px;
            font-size: 16px;
            border: none;
            cursor: pointer;
        }

        .dropdown {
            position: relative;
            display: inline-block;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f9f9f9;
            min-width: 160px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
        }

        .dropdown-content a {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }

        .dropdown-content a:hover {background-color: #f1f1f1}

        .dropdown:hover .dropdown-content {
            display: block;
        }

        .dropdown:hover .dropbtn {
            background-color: lightgray;
        }
    </style>
</head>
<body>
<div class="w3-container">

<img src="crawler.gif" alt="Crawler" style="float:left; width:75px; height:50px; border:0; ">
    <ul class="w3-navbar w3-grey" style="font-size: x-large">
        <li><a href="management.jsp">Management</a></li>
        <li><a href="statistic.jsp">Statistic</a></li>
        <li style="float: right"><a href="profile.jsp">User's menu </a> </li>

        <li style="float:right">

            <c:choose>
                <c:when test="${sessionScope.securityToken != null}">
                    <a href="<c:url value='profile.jsp' />"><c:out value='${sessionScope.securityToken.getUserData().getUserName()}'/></a>
                </c:when>
                <c:otherwise>
                    <a href="<c:url value='index.jsp' />">Guest</a>
                </c:otherwise>
            </c:choose>

        </li>

    </ul>

</div>
</body>
</html>

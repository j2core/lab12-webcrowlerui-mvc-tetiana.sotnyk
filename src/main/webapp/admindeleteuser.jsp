<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%--
  Created by IntelliJ IDEA.
  User: sts
  Date: 3/22/17
  Time: 4:42 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin Delete User</title>
    <style>
        p.thick {
            font-weight: bold;
        }
    </style>
</head>
<body>
<%@include file="header.jsp" %>
<%@include file="dbconnect.jsp" %>

<nav class="w3-sidenav w3-white w3-card-2" style="width:15%">
    <h4>Management:</h4>
    <a href="#">Manege nodes</a>
    <a style="background-color: lightgray" href="usersmanage.jsp">Manage users</a>
    <br>
    <a href="LogoutServlet">Logout</a>
    <a href="index.jsp">Home</a>

</nav>

<div class="w3-container" style="margin-left: 15%">

    <sql:query dataSource="${snapshot}" var="adminRole">
        SELECT * from rolesGroup WHERE groupId = 1;
    </sql:query>

    <c:set var="contains" value="false" scope="page"/>

    <c:forEach var="row" items="${adminRole.rows}">

        <c:set var="contains" value="false" scope="page"/>

        <c:forEach var="entity" items="${sessionScope.securityToken.permission}">

            <c:if test="${entity.groupName eq row.groupName }">

                <c:set var="contains" value="true" scope="page"/>

            </c:if>

        </c:forEach>

    </c:forEach>

    <c:if test="${contains}">

        <h4> Please write information about user which need delete from system(you may write only one field):</h4>

        <form action="AdminFindUserServlet" method="post">

            <input type="hidden" name="jspPage" value="admindeleteuser.jsp">

            User's Id:<br>
            <input type="text" name="userId"/><br>

            User's login:<br>
            <input type="text" name="login"/><br>

            User's name:<br>
            <input type="text" name="name"/><br><br>

            <input type="submit" name="submit"/>
        </form>

        <c:if test="${sessionScope.userToken eq sessionScope.securityToken}">

            <p style="color:red"> You can't delete your account.</p>

        </c:if>

        <c:if test="${sessionScope.exception}">

            <p style="color: red"> Something wrong! Please try again!</p>
            ${sessionScope.remove("exception")}

        </c:if>

        <c:if test="${sessionScope.remoteUser == true}">

            <h4 style="color: blue">Delete user was successfully!</h4>

            ${sessionScope.remove("remoteUser")}

        </c:if>

        <c:if test="${sessionScope.userToken != null}">

            <p class="thick"> User name:</p>
            <c:out value='${sessionScope.userToken.userData.userName}'/><br><br>
            <p class="thick"> User login: </p>
            <c:out value='${sessionScope.userToken.userData.login}'/><br><br>
            <p class="thick"> User permission: </p>
            <c:out value='${sessionScope.userToken.toPermissionString()}'/><br><br>

            <c:if test="${!(sessionScope.userToken eq sessionScope.securityToken)}">

                <form action="AdminDeleteUserServlet" method="post">

                    <input type="submit" name="delete user"/>

                </form>

            </c:if>

        </c:if>

        <c:if test="${sessionScope.exception}">

            <p style="color: red"> Something wrong! Please try again!</p>
            ${sessionScope.remove("exception")}

        </c:if>


    </c:if>

    <c:if test="${!contains}">

        <p style="color: red">You do not have enough rights to be present in this part of the site.</p>

    </c:if>

</div>

<%@include file="footer.jsp" %>
</body>
</html>

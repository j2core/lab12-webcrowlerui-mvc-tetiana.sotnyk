<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%--
  Created by IntelliJ IDEA.
  User: sts
  Date: 3/17/17
  Time: 5:38 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Manage node's</title>
</head>
<body>

<%@include file="header.jsp" %>
<%@include file="dbconnect.jsp" %>

<nav class="w3-sidenav w3-white w3-card-2" style="width:15%">
    <h4>Management:</h4>
    <a style="background-color: lightgray" href="nodesmanage.jsp">Manege nodes</a>
    <a href="usersmanage.jsp">Manage users</a>
    <br>
    <a href="LogoutServlet">Logout</a>
    <a href="index.jsp">Home</a>

</nav>

<div class="w3-container" style="margin-left: 15%">

    <c:if test="${sessionScope.stopNode == false}">

        <p style="color: red">  Some thing wrong with stopping your node. Please try again or report to the administration! </p>

        <c:remove var="stopNode" scope="session"/>

    </c:if>

    <h2> Node's information:</h2>

    <sql:query dataSource="${snapshot}" var="nodes">
        SELECT * from nodeData;
    </sql:query>
    <table border="2" width="80%">
        <tr>
            <th>Node's Id</th>
            <th>Start time</th>
            <th>Stop time</th>
            <th>Node's status</th>
            <th>Stop node</th>
        </tr>

        <c:forEach var="node" items="${nodes.rows}">

            <tr>

                <td>
                    <c:out value="${node.nodeId}"/>

                </td>

                <c:set var="nodeStatus" value="1" scope="page"/>

                <jsp:useBean id="startDate" class="java.util.Date"/>
                <jsp:setProperty name="startDate" property="time" value="${node.startUnixTime}"/>
                <td><fmt:formatDate value="${startDate}" pattern="dd/MM/yyyy hh:mm:ss"/></td>

                <c:if test="${node.stopUnixTime > 0}">
                    <jsp:useBean id="stopDate" class="java.util.Date"/>
                    <jsp:setProperty name="stopDate" property="time" value="${node.stopUnixTime}"/>
                    <td><fmt:formatDate value="${stopDate}" pattern="dd/MM/yyyy hh:mm:ss"/></td>
                    <c:set var="nodeStatus" value="-1" scope="page"/>
                </c:if>
                <c:if test="${node.stopUnixTime == 0}">
                    <td><c:out value="00:00:00"/></td>
                </c:if>

                <sql:query dataSource="${snapshot}" var="nodesUrl">
                    SELECT * from urlData where nodeId = ? and status = 1 limit 1
                    <sql:param value="${node.nodeId}"/>
                </sql:query>
                
                <c:if test="${nodesUrl.rowCount < 1 && node.stopUnixTime == 0}">
                    <c:set var="nodeStatus" value="0" scope="page"/>
                </c:if>

            <c:forEach var="url" items="${nodesUrl.rows}">

                <fmt:parseNumber var="changeTime" type="number" value="${url.statusChangeTime}"/>
                <c:set var="expTime" value="${(changeTime/1000) + 3600}"/>
                <jsp:useBean id="current" class="java.util.Date"/>
                <c:set var="time" value="${(current.time)/ 1000}"/>
                <c:if test="${expTime < time}">
                    <c:set var="nodeStatus" value="0" scope="page"/>
                </c:if>

            </c:forEach>

                <td>
                    <c:choose>
                        <c:when test="${nodeStatus == 1}">
                            <c:out value="node is working"/>
                        </c:when>
                        <c:when test="${nodeStatus == -1}">
                            <c:out value="node was stopped"/>
                        </c:when>
                        <c:when test="${nodeStatus == 0}">
                            <c:out value="node is frozen or was not correctly stopped"/>
                        </c:when>
                        <c:otherwise>
                            <c:out value="error"/>
                        </c:otherwise>
                    </c:choose>
                </td>

                <td>
                    <c:if test="${nodeStatus > -1}">
                        <form action="StopNodeServlet" method="post">
                            <button type="submit" name="nodeId" value="${node.nodeId}"> stop node</button>
                        </form>
                    </c:if>
                </td>

            </tr>

            <c:remove var="nodeStatus"/>

        </c:forEach>

    </table>

    <br><br>
    <form action="CreateNewNodeServlet" method="post">
        <input type="submit" value="create new node"/>
    </form>
    <form action="nodesmanage.jsp">
        <input type="submit" value="renew page"/>
    </form>
</div>

<%@include file="footer.jsp" %>

</body>
</html>
